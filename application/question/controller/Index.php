<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\Question\controller;
use app\common\controller\Base;
class Index extends Base
{
    public function index()
    {

        $id = $this->request->only(['id']);
        $status = $this->request->only(['status']);
        $status = $status['status'];
        $id = (int) $id['id'];

        if(!$id){
            // $list = model('Question')->getList($status);
            switch ($status) {
                case 'hot':
                   $order = "views_count desc";
                    break;
                case 'recommend':
                   $order ="is_recommend desc";   
                    break;
               case 'unresponsive':
                   $order ="a.answer_count asc";   
                    break;
                default:
                    $order="published_uid desc";
                    break;
            }

            // 列表
            // show(getset('contents_per_question'));
             $list = model('base')->getpages('question',['page'=>getset('contents_per_question'),'join'=>[[config('prefix').'users us','a.published_uid=us.uid']],'alias'=>'a','field'=>'a.*,us.user_name,us.avatar_file','order'=>$order,'cache'=>false]);
             // show($list);
             //topic
             foreach ($list as $key => $v) {
                 $topics[$v['question_id']]= model('base')->getall('topic_relation',['join'=>[[config('prefix').'topic tpc','tpc.topic_id=tpc_rela.topic_id']],'alias'=>'tpc_rela','where'=>['tpc_rela.item_id'=>"{$v['question_id']}",'type'=>"question"]]);
             }
             //热门用户
             // $hostuser = model('base')->query('select * from '.config('prefix').'question')
             
             $this->assign('topics',$topics);
            $this->assign('status',$status);
            $this->assign('list',$list);
        }else{
            // 内容和发表人
            $this->assign($question = model('Question')->getDetailById($id));
            //话题
            $this->assign('topic',$topic = model('Question')->getTopicById($id));
            //回答
            $this->assign('answer',$answer=model('base')->getall('question_comments',['where'=>['question_id'=>$id],'join'=>[[config('prefix').'users us','qucmes.uid=us.uid']],'alias'=>'qucmes','field'=>'qucmes.*,us.user_name,us.avatar_file','order'=>'time desc']));
            // show($answer);
        }
		  //当前用户登录权限
            if ($this->getuid()){
              $userInfo=model('Users')->getUserByUid($this->getuid());
            if ($userInfo['group_id'] == 1){
                $this->assign('permission',true);
            }
            //用户是否关注当前问题
            $data['question_id'] = $id;
            $data['uid'] = $this->getuid();
            $focus = model('Focus')->get_focus_st($data);
            if (!empty($focus)){
                $this->assign('focus',1);
            }else{
                $this->assign('focus',2);
            }
            }
       $setting = cache('system_setting');
        $tpl=$id?"question":'question_list';
        //seo
        $seo['title'] = $id>1?$question['question_content']."-".unserialize($setting[1]['value']):"问题列表-".unserialize($setting[1]['value']);
        $seo['description'] = $id>1?msubstr(strip_tags($question['question_detail']),0,50):unserialize($setting[2]['value']);
        $seo['keywords'] = $id>1?msubstr(strip_tags($question['question_detail']),0,50):unserialize($setting[3]['value']);
        $this->assign('seo',$seo);

        //相关问题
        $this->assign('aboutquestion',$this->getbase->getall('question',['field'=>'question_content,question_id,answer_count,view_count','order'=>'question_id desc','limit'=>'10']));
        $this->assign('hotusers',$this->getbase->getall('users',['field'=>'uid,user_name,avatar_file','order'=>'uid desc','limit'=>'20']));
        $this->assign('hottags',$this->getbase->getall('topic',['field'=>'topic_id,topic_title','order'=>'topic_id desc','limit'=>'20']));
       return $this->fetch('question/'.$tpl);  
        
    }

}
