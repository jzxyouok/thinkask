<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\Question\controller;
use app\common\controller\Base;
class Ajax extends Base
{
	public function _initialize()
    {
        //用户是否登陆
        if (!$this->getuid() || !$this->request->isAJax()) {
            return;
        }
    }
    public function edit()
    {

        $id = (int)current($this->request->only(['id']));
        //字段判断
        if ($question_content = $this->request->only(['question_content'])) {
            if (empty($question_content['question_content'])) {
                $this->error('标题不能为空');
            }
        }
        if ($category_id = $this->request->only(['category_id'])) {
            if (empty($category_id['category_id']) || $category_id['category_id'] < 1) {
                $this->error('请选择分类');
            }
        }
        $ardb = $this->request->param();
        $ardb['add_time'] = time();
        $ardb['category_id'] = $ardb['category_id'];
        $ardb['published_uid'] = $this->getuid();
        $ardb['question_content']=htmlspecialchars($ardb['question_content']);
        $ardb['question_detail']=htmlspecialchars($ardb['question_detail']);

        if ($id) {
            //修改
            model('Base')->getedit('question', ['where' => "question_id=$id"], $ardb);
            model('Base')->getedit('posts_index', ['where' => "post_id=$id"], ['update_time' => time()]);
            $this->success('操作成功', "/question/$id");
        } else {
            // 新加
            $id = model('Base')->getadd('question', $ardb);
            if ($id) {
                //POST_INDEX
                $data['post_id'] = $id;
                $data['post_type'] = "question";
                $data['add_time'] = $ardb['add_time'];
                $data['update_time'] = time();
                //是否匿名
                // $data['anonymous']
                $data['uid'] = $this->getuid();
                model('Base')->getadd('posts_index', $data);
                // echo "string"
                $this->success('操作成功', "/question/$id");

            }
        }
    }
    /**
         * 保存评论
         */
        public function comment()
        {
            //测试关闭校验ajax
            if ($this->request->isAJax()) {
                $comm = $this->request->param();
                $comm['question_id'] = (int)$comm['question_id'];
                $comm['message'] = $comm['message'];
                $comm['uid'] = $this->getuid();
                if (empty($comm['question_id'])) {
                    return ;
                }
                if (empty($comm['message'])){
                	$this->error('您没有填写回答内容哦!');
                }
                // if()
                //查询用户是否已经评论过此问题
                // $Comment = model('Comments')->get_comment_content($this->getuid(), $comm['question_id']);
                  if (model('Comments')->save_comment_content($comm)) {
                  		$this->success('回答成功!');
                        
                    }else{
                    	$this->error('回答提交失败了,请稍后重试吧!');
                    }
            } else {
                $this->error('非法请求!');
            }
        }
      
		//临时注释
        //用户关注/取消问题
        public  function focus()
        {
            if ($this->request->isAJax()) {
                $data=$this->request->param();
                $data['question_id'] =(int)$data['question_id'];
                $data['uid'] = $this->getuid();
                $result = model('Focus')->get_focus_st($data);
                if (empty($result)) {
                    $id = model('Focus')->focus($data);
                    if ($id) {
                        $this->success('关注成功');
                    } else {
                        $this->error('关注失败');
                    }
                } else {
                    $status = model('Focus')->unfocus($data);
                    if ($status === 1) {
                        $this->success('取消关注成功');
                    } else {
                        $this->error('取消怎么失败了呢?');
                    }
                }
            }else{
                $this->error('非法请求');
            }
        }

}
