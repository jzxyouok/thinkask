<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */

namespace app\systems\controller;
use app\common\controller\Base;
class Ajax extends Base
{

    // public function _initialize() {
           
    // }
    public function delete(){
         if($this->request->isAJax()&&$this->getuid()>0){
            $data = $this->request->param();
            $wheres = explode("-", decode($data['where']));
            $where[$wheres[0]] =$wheres[1]; 
            model('Base')->getdel(decode($data['table']),['where'=>$where]);
            $this->success('删除成功');
        }
    }
    public function add(){
            if($this->request->isAJax()){
               $data = $this->request->param();
                if(model('Base')->getadd(decode($data['table']),$data)){
                    $this->success('添加成功',$data['returnurl']);
                }else{
                    $this->error('添加错误');
                } 
            }
            

        }
     public function edit(){
            if($this->request->isAJax()){
               $data = $this->request->param();
               $wheres = explode("-", decode($data['where']));
                $where[$wheres[0]] =$wheres[1]; 
                unset($data[$wheres[0]]);
                // show($where);
                if(model('Base')->getedit(decode($data['table']),['where'=>$where],$data)!==flase){
                    $this->success('修改成功',$data['returnurl']);
                }else{
                    $this->error('添加错误,检查是否有修改内容');
                } 
            }
            
        }
}
