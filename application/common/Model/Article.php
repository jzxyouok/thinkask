<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\model;
use think\Model;
use think\Db;
class Article extends Model
{  
    public function edit($data){
    	$data['message'] = htmlspecialchars($data['message']);
    	$data['title'] = htmlspecialchars($data['title']);
    	$data['article_id']  = (int)$data['article_id'];
        $data['add_time'] = time();
        
    	if($data['article_id']>0){
    		return $this->publish($data);
    	}else{
    		return $this->add($data);
    	}
    }

    private function publish($data){

    }
    public function getArById($id){
        $join = [
                    [config('prefix').'users us','a.uid=us.uid'],
                    // ['think_card c','a.card_id=c.id'],
                ];
        return Db::name('article')->alias('a')->join($join)->where('a.id',$id)->find();
    }

    /**
     * 获取数据表所有内容
     */
    public function getAllList($table)
    {
    	return Db::name($table)->select();
    }

}