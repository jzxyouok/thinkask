<?php 
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\behavior;
use app\common\controller\Base;
use \think\Request;
use \think\Session;
use \think\Cookie;
use \think\Hook;
use \think\Lang;
use \think\Config;
// echo "当前模块名称是" . $request->module();
// echo "当前控制器名称是" . $request->controller();
// echo "当前操作名称是" . $request->action();
class Exsns extends Base
{
    public function run(&$params)
    {
        $this->initconfig();
    }

    private function initconfig(){
        $config = [
        //git.oschina.net配置
        'SDK_GIT' => [
            'APP_KEY' => 'fK2l0B5mGyLzQzYH3pTs', //应用注册成功后分配的 APP ID
            'APP_SECRET' => 'Yd4cgWPh60vmxXeakNvj5i8hNusn27c1', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl(). 'git',
        ],
        //有道笔记 Youdao
        'SDK_YOUDAO' => [
            'APP_KEY' => '37a4eea74a7e53ebe4845d03d2185695', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '50a5cb7c5549412193bffa192411b5fa', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'youdao',
        ],



       'TMPL_ACTION_ERROR'     =>  '/success', // 默认错误跳转对应的模板文件
        'TMPL_ACTION_SUCCESS'   =>  '/success', // 默认成功跳转对应的模板文件


        //腾讯QQ登录配置
        'SDK_QQ' => [
            'APP_KEY' => getset('qq_login_app_id'), //应用注册成功后分配的 APP ID
            'APP_SECRET' => '17a5a055e69bb12e3c880ca79049e901', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'qq',
        ],
        //新浪微博配置
        'SDK_SINA' => [
            'APP_KEY' => '3453155715', //应用注册成功后分配的 APP ID
            'APP_SECRET' => 'a09ddb87450a2d65337c8334dd99b810', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'sina',
        ],
        
        //支付宝登录
        'SDK_ALIPAY' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'alipay',
        ],
        //微信登录
        'SDK_WEIXIN' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'weixin',
        ],
        
        //腾讯微博配置
        'SDK_TENCENT' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'tencent',
        ],
        
        //网易微博配置
        'SDK_T163' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 't163',
        ],
        //人人网配置
        'SDK_RENREN' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'renren',
        ],
        //360配置
        'SDK_X360' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'x360',
        ],
        //豆瓣配置
        'SDK_DOUBAN' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'douban',
        ],
        //Github配置
        'SDK_GITHUB' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'github',
        ],
        //Google配置
        'SDK_GOOGLE' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'google',
        ],
        //MSN配置
        'SDK_MSN' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'msn',
        ],
        //点点配置
        'SDK_DIANDIAN' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'diandian',
        ],
        //淘宝网配置
        'SDK_TAOBAO' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'taobao',
        ],
        //百度配置
        'SDK_BAIDU' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'baidu',
        ],
        //开心网配置
        'SDK_KAIXIN' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'kaixin',
        ],
        //搜狐微博配置
        'SDK_SOHU' => [
            'APP_KEY' => '', //应用注册成功后分配的 APP ID
            'APP_SECRET' => '', //应用注册成功后分配的KEY
            'CALLBACK' => getSiteUrl() . 'sohu',
        ],
        ];
        Config::set($config);
        config('prefix','thinkask_');
    }
 

}