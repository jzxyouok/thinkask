<?php 
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
//http://thinkask.com/plus/demo/index/index
namespace app\common\behavior;
use app\common\controller\Base;
class Doplus extends Base
{
    public function run(&$params)
    {
      \think\Loader::addNamespace('plus','../plus/');

      if(strtolower($this->request->controller())=="plus"&&strtolower($this->request->module())!="admin"){
        //目录
        $plusname = strtolower(tostr($this->request->only(['plusname'])));
        $controller = strtolower(tostr($this->request->only(['controller'])));
        $action = strtolower(tostr($this->request->only(['action'])))?strtolower(tostr($this->request->only(['action']))):"index";
        if(!$plusname||!$controller){
          $this->error2('/plus/demo/index/index{plus后面三个参数1，插件名称;2,插件控制器名;3,方法名}');
        }
        $path = "\plus\\$plusname\controller\\$controller";
        $plus = new  $path();
        $plus->$action();
        exit();
      }

    }



}