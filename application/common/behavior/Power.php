<?php 
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\behavior;
use \think\Controller;
use \think\Request;
use \think\Session;
use \think\Cookie;
use \think\Hook;
use \think\Lang;
// echo "当前模块名称是" . $request->module();
// echo "当前控制器名称是" . $request->controller();
// echo "当前操作名称是" . $request->action();
class Power extends Controller
{
    protected   $request;
    public function run(&$params)
    {

        $this->checkSession();
        
        $this->request = Request::instance();
        //当前的模型  
        $module = strtolower($this->request->module());
        $action = strtolower($this->request->action());
        $this->assign('domodule',$module);
        $this->assign('doaction',$action);
        if($module=="admin"){
            if(!session('thinkask_uid')){
                $this->error('您没有后台操作权限');
            }
        }
        

    }
    private function checkSession(){
    	if(session('thinkask_uid')){
            $userinfo = model('users')->getUserByUid(Session::get('thinkask_uid'));
            if($this->request->module()=="admin"&&$userinfo['group_id']!=1){
                $this->error('您没有操作权限');
            }
    	}else{
            $userinfo['uid'] = 0;
    	}
        $this->assign('userinfo',$userinfo);
    }


}