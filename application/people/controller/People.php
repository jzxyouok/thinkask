<?php
//SanSanYun.Com [Don't forget the beginner's mind]
//Copyright (c) 2016~2099 http://SanSanYun.com All rights reserved.
//Author: By 茶茶
namespace app\people\controller;
use app\common\controller\Base;

class People extends Base
{
    public function index(){

        $data=$this->request->param();
        if ('index' == $data['id']){
            $data['id'] = session('thinkask_uid');
        }
        if($data['id'] == session('thinkask_uid')){
            $this->assign('edit_avatar',1);
        }else{
            $this->assign('edit_avatar',0);
        }
        $this->assign(model('Users')->getUserByUid($data['id']));
        $this->assign('fans',model('Follow')->fans_count($data['id']));
        $this->assign('focus',model('Follow')->friend_count($data['id']));

       // $question_list = db('Question')->where('published_uid',$data['id'])->order('add_time desc')->limit(12)->select();
        $question_list = model('Question')->getAnswerByUid($data['id']);
        $this->assign('question_list',$question_list);
        return $this->fetch('people/people');
    }

    public function people_list(){
        $this->assign('list',$this->getbase->getpages('users'));
        return $this->fetch('people/people_list');
    }

	/**
     * [上传头像]
     * @return [type] [description]
     */
	public function upload_avatar(){
		$uid = $this->getuid();
		if(empty($uid)){
		$this->error('请登录后重试','../ucenter/user/login');
		}
	// avatar 是上传name的值
    $file = request()->file('avatar');
	//验证图片大小,格式
	$max_size=getset('upload_avatar_size_limit');
    // 移动到应用根目录/public/uploads/avatar 目录下,相同名称->覆盖
	$result = $file->validate(['ext'=>'jpg,png,gif','size' => 102400000])->move(ROOT_PATH . 'public' . DS . 'uploads'.DS.'avatar','avatar_'.$uid);
	//获取用户id
    if($result){
        // 成功上传后 获取上传信息返回json
		//是否更新到users表
		$data['uid'] = $uid;
		$data['avatar_file'] = '/uploads/avatar/'.$this->returnpath.$result->getSaveName();
		$up_result=model('Users')->update_avatar($data);
		$this->success('上传头像成功');
    }else{
        // 上传失败获取错误信息
		 $this->error($file->getError(),'/people/index');
		}
	}
}