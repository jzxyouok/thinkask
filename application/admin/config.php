<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */

return [

    // +----------------------------------------------------------------------
    // | 模板设置
    // +----------------------------------------------------------------------

    'template'               => [
        // 模板引擎类型 支持 php think 支持扩展
        'type'         => 'Think',
        // 模板路径
        'view_path'    => '../template/default/',
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板文件名分隔符
        'view_depr'    => DS,
        // 模板引擎普通标签开始标记
        'tpl_begin'    => '{',
        // 模板引擎普通标签结束标记
        'tpl_end'      => '}',
        // 标签库标签开始标记
        'taglib_begin' => '{',
        // 标签库标签结束标记
        'taglib_end'   => '}',
    ],
'adminmenu'               => [
    // 0=>[
    //     // 首页
    //     'name'=>'admin_menu_index',
    //     'ico'=>'',
    //     'url'=>'',
    //     'child'=>[]
    // ],
    1=>[
        // 全局设置
        'name'=>'admin_glob_set',
        'ico'=>'fa-cog',
        'url'=>'',
        'child'=>[
            [
            // 站点信息
            'name'=>'admin_web_set',
            'ico'=>'',
            'url'=>'/admin/setting/set/',
            'url_extra'=>'?status=site',
                ],

            [
            // 注册访问
            'name'=>'admn_reg_view',
            'ico'=>'',
            'url'=>'/admin/setting/set/',
            'url_extra'=>'?status=register',
                ],
            [
            // 站点功能
            'name'=>'admin_web_function',
            'ico'=>'',
            'url'=>'/admin/setting/set/',
            'url_extra'=>'?status=functions',
                ],
            [
            // 内容设置
            'name'=>'admin_content_set',
            'ico'=>'',
            'url'=>'/admin/setting/set/',
            'url_extra'=>'?status=contents',
                ],
            // [
            // // 导航设置
            // 'name'=>'admin_navigation_set',
            // 'ico'=>'',
            // 'url'=>''
            //     ],
            [
            // 分类设置
            'name'=>'admin_category_set',
            'ico'=>'',
            'url'=>'/admin/category/index'
                ],
            //  4=>[
            // // 专题设置
            // 'name'=>'admin_subject_set',
            // 'ico'=>'',
            // 'url'=>''
            //     ],
            //  5=>[
            // // 页面设置
            // 'name'=>'admin_page_set',
            // 'ico'=>'',
            // 'url'=>''
            //     ],
            //  6=>[
            // // 帮助中心
            // 'name'=>'admin_help',
            // 'ico'=>'',
            // 'url'=>''
            //     ], 
            //  5=>[
            // // 威望积分
            // 'name'=>'admin_score',
            // 'ico'=>'',
            // 'url'=>'/admin/setting/set/',
            // 'url_extra'=>'?status=integral',
            //     ],
            //  6=>[
            // // 用户权限
            // 'name'=>'admin_user_power',
            // 'ico'=>'',
            // 'url'=>'/admin/setting/set/',
            // 'url_extra'=>'?status=permissions',
            //     ],
             7=>[
            // 邮件设置
            'name'=>'admi_mail_set',
            'ico'=>'',
            'url'=>'/admin/setting/set/',
            'url_extra'=>'?status=mail',
                ],
             8=>[
            // 开放平台
            'name'=>'admin_open',
            'ico'=>'',
            'url'=>'/admin/setting/set/',
            'url_extra'=>'?status=openid',
                ],
            //  9=>[
            // // 性能优化
            // 'name'=>'admin_performance_optimization',
            // 'ico'=>'',
            // 'url'=>'/admin/setting/set/',
            // 'url_extra'=>'?status=cache',
            //     ],
             10=>[
            // 界面设置
            'name'=>'admin_visual_set',
            'ico'=>'',
            'url'=>'/admin/setting/set/',
            'url_extra'=>'?status=interface',
                ],
        ],
    ],
    'adminmenu'=>[
        // 内容管理
        'name'=>'admin_content_model',
        'ico'=>'fa-puzzle-piece',
        'url'=>'',
        'child'=>[
            
         
            
        ],
    ],
    'plus'=>[
        'name'=>'admin_plus_model',
        'ico'=>'fa-random',
        'url'=>'',
        'child'=>[
        [
            // 站点信息
            'name'=>'admin_plus_list',
            'ico'=>'',
            'url'=>'/admin/addon/list/',
            'url_extra'=>'',
                ] 
        ],
    ],
    3=>[
        // 用户中心
        'name'=>'admin_user_center',
        'ico'=>'fa-group',
        'url'=>'',
        'child'=>[
            [
            // 用户列表
            'name'=>'admin_user_list',
            'ico'=>'',
            'url'=>'admin/user/index'
                ],
            [
            // 用户组
            'name'=>'admin_user_group',
            'ico'=>'',
            'url'=>'admin/user/group'
                ],
            // [
            // // 批量邀请
            // 'name'=>'admin_invitation',
            // 'ico'=>'',
            // 'url'=>'admin/user/groupinvites'
            //     ],
            //  [
            // // 职位管理
            // 'name'=>'admin_job_management',
            // 'ico'=>'',
            // 'url'=>'admin/user/job'
            //     ],
        ],
    ],
    //  4=>[
    //     // 审核管理
    //     'name'=>'admin_audits_management',
    //     'ico'=>'',
    //     'url'=>'',
    //     'child'=>[
    //         1=>[
    //         // 认证审核
    //         'name'=>'admin_authentication_audit',
    //         'ico'=>'',
    //         'url'=>'admin/approval/content'
    //             ],
    //         2=>[
    //         // 注册审核
    //         'name'=>'admin_reg_audit',
    //         'ico'=>'',
    //         'url'=>'admin/approval/user'
    //             ],
    //          4=>[
    //         // 用户举报
    //         'name'=>'admin_user_accusation',
    //         'ico'=>'',
    //         'url'=>'admin/approval/report_list'
    //             ],
    //     ],
    // ],
 
    // 5=>[
    //     // 第三方扩展开发
    //     'name'=>'admin_sns_api',
    //     'ico'=>'fa-code-fork',
    //     'url'=>'',
    //     'child'=>[
    //         1=>[
    //         // 微信菜单
    //         'name'=>'admin_wexin_menu',
    //         'ico'=>'',
    //         'url'=>''
    //             ],
    //         2=>[
    //         // 微信自定义回复
    //         'name'=>'admin_wexin_reply',
    //         'ico'=>'',
    //         'url'=>''
    //             ],
    //          4=>[
    //         // 专题设置
    //         'name'=>'admin_subject_set',
    //         'ico'=>'',
    //         'url'=>''
    //             ],
    //          5=>[
    //         // 页面设置
    //         '微信第三方接入'=>'admin_wexin_api',
    //         'ico'=>'',
    //         'url'=>''
    //             ],
    //          6=>[
    //         // 微信二维码管理
    //         'name'=>'admin_wexin_code',
    //         'ico'=>'',
    //         'url'=>''
    //             ],  
    //          7=>[
    //         // 微信消息群发
    //         'name'=>'admin_wexin_msg_group',
    //         'ico'=>'',
    //         'url'=>''
    //             ],  
    //          8=>[
    //         // 微博消息接收
    //         'name'=>'admin_webo_msg_get',
    //         'ico'=>'',
    //         'url'=>''
    //             ],
    //          9=>[
    //         // 邮件导入
    //         'name'=>'admin_mail_import',
    //         'ico'=>'',
    //         'url'=>''
    //             ],
    //     ],
    // ],




],


];
