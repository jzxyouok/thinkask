<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\admin\controller;
use think\Controller;

class Index extends controller
{
  public function index(){
  	//菜单
  	$defaultmenu = config('adminmenu');
  
  	$Models = finddirfromdir("./../application");
  	$no_join = ['ajax','index','asset','common','post','ucenter','admin'];
    //模型
  	$Mmenu = [];
      	foreach ($Models as $k => $v) {
      		$path = "./../application/".$v."/menu.php";
      		if(!in_array($v, $no_join)&&file_exists($path)){
      			$menu= include($path);
      			$Mmenu[]=$menu['adminmenu'];
      			
      		}
    	}
    //插件
    $Plus = finddirfromdir("./../plus");
    if(is_array($Plus)){
      foreach ($Plus as $k => $v) {
        $path = "./../plus/".$v."/config.php";
        if(file_exists($path)){
          $menu= include($path);
          if($menu['plusmenu']){
            $defaultmenu['plus']['child'][]=$menu['plusmenu'];
          }
          
        }

      }
    }
    // show($defaultmenu);
    // die;
  	$defaultmenu['adminmenu']['child']=$Mmenu;
     // show($defaultmenu);
     // die;
  	$this->assign('menu',$defaultmenu);

  	return $this->fetch('admin/index/index');
  }
  public function toMain(){
  	return $this->fetch('admin/index/tomain');
  }
}
