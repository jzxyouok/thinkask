<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\Admin\controller;
use app\common\controller\Base;
class Category extends Base
{
 /**
  * [index 用户管理]
  * @return [type] [description]
  */
  public function index(){
    $this->assign('list',$this->getbase->getall('category',['order'=>'sort desc,id desc']));
    return $this->fetch('admin/category/index');
   }
  public function edit(){
  	$this->assign('category',$this->getbase->getall('category',['order'=>'id desc','field'=>'id,title']));
  	$id = $this->request->only(['id']);
  	if($id){
  		$this->assign($this->getbase->getone('category',['where'=>$id]));

  	}
  	return $this->fetch('admin/category/edit');
  }

 

}
