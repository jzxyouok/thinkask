<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\admin\controller;
use app\common\controller\Base;
class User extends Base
{
 /**
  * [index 用户管理]
  * @return [type] [description]
  */
  public function index(){
  	//组列表
  	$group = model('Base')->getall('users_group',['cache'=>false]);
  	foreach ($group as $k => $v) {
  		$group[$k]['count'] = model('Base')->getcount('users',['cache'=>false,'where'=>['group_id'=>$v['group_id']]]);
  	}
   $this->assign('group',$group);
  	//当前组下面的用户
   $group_id = (int) $this->arrTstr($this->request->only(['group_id']),'group_id');
   if($group_id>0){
   		$this->assign('users',model('Base')->getall('users',['where'=>['group_id'=>$group_id]]));
   		$this->assign('group_id',$group_id);
   }

  	return $this->fetch('admin/user/index');
  }
  public function edit(){
    $id = $this->request->only(['id']);
    $id = (int)$id['id'];
    if($id>0){
      $this->assign($users = model('Base')->getone('users',['where'=>['uid'=>$id],'cache'=>false]));
      $this->assign($users_attrib = model('Base')->getone('users_attrib',['where'=>['uid'=>$id],'cache'=>false]));
    }
    $this->assign('jobs',$province=model('Base')->getall('jobs'));
    $this->assign('category',$category=model('Base')->getall('users_group'));
   return $this->fetch('admin/user/edit');
  }
  /**
   * [group 组管理]
   * @return [type] [description]
   */
  public function group(){
  		//系统组
  	 $this->assign('group',$group = model('Base')->getall('users_group'));
  	return $this->fetch('admin/user/group');
  }
  /**
   * [invites 批量邀请]
   * @return [type] [description]
   */
  public function invites(){
  	return $this->fetch('admin/user/invites');
  }
  /**
   * [job 职位管理]
   * @return [type] [description]
   */
  public function job(){

  	$this->assign('job',$job=model('Base')->getall('jobs'));
  	// show($job);
	return $this->fetch('admin/user/job');
  }
 

}
