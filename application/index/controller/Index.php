<?php
/*
+--------------------------------------------------------------------------
|   thinkask [#开源系统#]
|   ========================================
|   http://www.thinkask.cn
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\index\controller;

use app\index\model\Index as Indexss;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
use think\Route;
use think\Hook ;
// use app\common\Hook;
class index extends Controller
{

	public function _initialize()
    {
        
    }
    public function index()
    {

        $setting = cache('system_setting');
        $seo['title'] = "首页-".unserialize($setting[1]['value']);
        $seo['description'] = unserialize($setting[2]['value']);
        $seo['keywords'] = unserialize($setting[3]['value']);
        $this->assign('seo',$seo);
        if($_SERVER['HTTP_HOST']=="thinkask.cn"||$_SERVER['HTTP_HOST']=="www.thinkask.cn"||$_SERVER['HTTP_HOST']=="thinkask.com"||$_SERVER['HTTP_HOST']=="www.thinkask.com"){
            return $this->fetch();
        }else{
           $this->redirect('/question/index');
        }
		
    }
    public function app(){
        return $this->fetch();
    }




}
